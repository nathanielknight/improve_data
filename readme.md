#IMPROVE data

Scripts for working with carbonaceous aerosol data from the IMPROVE network. The two main things I do with this data are analyze/visualize it directly and prepare input files for my GEOS-Chem adjoint assimilation with it.

`helpers.py` contains some common functions for working with the data.

I also make use of my [data helpers](http://github.com/neganp/data_helpers).

##Downloading
IMPROVE data can be downloaded from their [webiste](http://vista.cira.colostate.edu/IMPROVE/). The fields I'm interested in are

 * Latitude
 * Longitude
 * Site Code
 * BC value, error, and detection limit
 * OC Value, error, and detection limit

I'm only interested in the last block of the data-file that gets downloaded (the "Data" block), which I've split out into it's own file. If the header is left intact the rest of my code can infer which field is which.

Presently I've got my data in the `raw/imprv_*` directories for the years I've downloaded, with the actual data saved in `data`.


##Pre-processing
Before analyzing the data, it's parsed into a database (I use `sqlite` for convenience). The scripts for doing this are:

  * `dbgen.py`: Reads non-negative values from the IMPROVE csv file into the database and save.
 * `dbfilter.py`: Replaces values below the MDL with MDL/2 (in the new table `valid_imprv`) and checks that sites fit a log-normal distribution.


##Analysis
Once the database has been generated, `dbstat.py` can be used to see its vital statistics. `distributions.py` creates exponentially binned histograms of concentrations by site. `draw_na_grid.py` draws the location of each site along with the GEOS-Chem 4x5 grid.

`trend.py` calculates and plots the average of seasonal IMPROVE data at each site along with errors. `seasonal_avgs.py`

`seasonal_averages.py` calculates and plots the seasonal average of BC and OC at each site.


##Adjoint Input

Excluded gridboxes can be urban sites, or areas of high biomass-burning influence (as determined in the forward run). These are stored in `banned_indices`.

`profiles.py` generates a daily average value for each grid-box in the GEOS-Chem grid. These get saved in `data/` and can be pushed to stetson (either by `push_to_stetson.sh` or in `all.sh`).


Profiles are in the format:
| date | (bc | oc) value | fractional uncertainty | i-index | j-index | 
