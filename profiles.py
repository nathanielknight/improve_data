#!/usr/bin/env python
'''
This script generates an "aerosol profile" which represents IMPROVE data in a
format compatible with the GEOS-Chem atmospheric transport model.

There are 3 parts.

* Parameters and Setup: settings for the spatial and temporal subset of
IMPROVE data to  assimilate into the profile.

* Averaging function: closure that pulls out space-and-time averaged values by
name.

* Output: Instructions for saving relevant data to files for the GEOS-Chem
adjoint to ingest.
'''

import sqlite3, itertools, sys
import gchem as gc



class ImprvProfile:
    "Configuration and methods for creating a BC/OC profile from IMPROVE data."

    #Configuration Constants
    IMPRV_UNC = 0.5
    II_START = 2 #bounding geos-chem indices
    II_STOP = 25
    JJ_START = 26
    JJ_STOP = 41
    LON_EDGES = gc.grids.e_lon_4x5 #bounding coordinates
    LAT_EDGES = gc.grids.e_lat_4x5
    LON_START = LON_EDGES[II_START]
    LON_STOP = LON_EDGES[II_STOP+1]
    LAT_START = LAT_EDGES[JJ_START]
    LAT_STOP = LAT_EDGES[JJ_STOP+1]

    def __init__(self, 
                 start_date=None,
                 end_date=None,
                 cur=None,
                 filter_indices=None):
        """Prepare a GC profile from start_date to end_date (as int::
        yyyymmdd) from the database poitned to by cur."""
        assert type(start_date) == int
        self.start_date = start_date
        assert type(end_date) == int
        self.end_date = end_date
        
        if type(cur) != sqlite3.Cursor:
            raise TypeError, "Need a database cursor, not {}".format(type(cur))
        self.cur = cur
        self.__create_restricted_tables(self.cur)
        
        if type(filter_indices) == "str":
            self.banned_indices = map(lambda x: tuple(map(lambda x: int(float(x)), x.split())),
                                      file(filter_indices_file, 'r'))
        elif type(filter_indices) == file:
            self.banned_indices = map(lambda x: tuple(map(lambda x: int(float(x)), x.split())), filter_indices_file)
        elif filter_indices is None:
            self.banned_indices = [] # nothing is in banned indices
        else: 
            raise TypeError, "Expecting a file or filename for filter_indices."

        
    def __create_restricted_tables(self, cur):
        cur.execute('''
        CREATE TEMP VIEW restricted_to_continent AS
        SELECT * from valid_imprv where 
        (lat > {0[lat_start]} AND lat < {0[lat_stop]}) AND
        (lon > {0[lon_start]} AND lon < {0[lon_stop]})
        '''.format( 
            {'lat_start': self.LAT_START, 'lat_stop': self.LAT_STOP,
             'lon_start': self.LON_START, 'lon_stop': self.LON_STOP}))
        cur.execute('''
        CREATE TEMP TABLE restricted_by_date AS
        SELECT * FROM restricted_to_continent WHERE
        CAST(date AS INT) >= {0[start_date]} AND 
        CAST(date AS INT) <= {0[end_date]}
        '''.format( 
            {'start_date': self.start_date,
             'end_date': self.end_date}))


    def avgs_by_site(self, value_field, unc_field):
        ''' Performs averages (grouped by date) for data in each GEOS-Chem
        cell in the above-restricted data. '''
        for i,j in itertools.product(
                range(self.II_START, self.II_STOP + 1),
                range(self.JJ_START, self.JJ_STOP + 1)):
            if (i,j) in self.banned_indices: 
                print "skipping index ", (i,j)
                continue
            else:
                lon_start, lon_stop = self.LON_EDGES[i], self.LON_EDGES[i+1]
                lat_start, lat_stop = self.LAT_EDGES[j], self.LAT_EDGES[j+1]
                data = self.cur.execute('''
                SELECT date, AVG({0[value_field]}), AVG({0[unc_field]})  FROM restricted_by_date
                WHERE
                (lat > {0[lat_start]} AND lat < {0[lat_stop]}) AND
                (lon > {0[lon_start]} AND lon < {0[lon_stop]})
                GROUP BY date;
                '''.format({'value_field': value_field, 'unc_field': unc_field,
                    'lat_start':lat_start, 'lat_stop':lat_stop,
                    'lon_start':lon_start, 'lon_stop':lon_stop})).fetchall()
                for date, value, unc in data:
                    if date:
                        yield (int(float(date)), value, (value*self.IMPRV_UNC), i,j)
                    else:
                        continue


    def avgs_by_year(self, value_field):
        for i,j in itertools.product(
                range(self.II_START, self.II_STOP+1),
                range(self.JJ_START, self.JJ_STOP+1)):
            lon_start, lon_stop = LON_EDGES[i], LON_EDGES[i+1]
            lat_start, lat_stop = LAT_EDGES[j], LAT_EDGES[j+1]
            data = cur.execute('''
        SELECT AVG({0[value_field]})  FROM restricted_by_date
        WHERE
        (lat > {0[lat_start]} AND lat < {0[lat_stop]}) AND
        (lon > {0[lon_start]} AND lon < {0[lon_stop]});
        '''.format({'value_field':value_field,
                    'lat_start':lat_start, 'lat_stop':lat_stop,
                    'lon_start':lon_start, 'lon_stop':lon_stop})).fetchall()[0][0]
            if data:
                yield i,j,data
            else:
                continue

    @staticmethod
    def save_to_file(data, filename):
        with file(filename, "w") as outfile:
            for datum in data:
                outfile.write("{: <10d} {} {} {} {}\n".format(*datum))

    def save_bc(self, filename):
        self.save_to_file(
            self.avgs_by_site("bc_val", "bc_unc"), filename)
        
        
    def save_oc(self, filename):
        self.save_to_file(
            self.avgs_by_site("oc_val", "oc_unc"), filename)

    def save_indices(self, filename):
        with file(filename, 'w') as outfile:
            for datum in set(((x[3], x[4]) for x in self.avgs_by_site('bc_val', 'bc_unc'))):
                outfile.write("{} {}\n".format(*datum))




#Output
if __name__ == "__main__":

    start_date, end_date = map(int, sys.argv[1:3])
    print("Doing profiles from {} to {}".format(start_date, end_date))

    print("Connecting to database")
    db = sqlite3.connect("data/sites.sqlite")
    cur = db.cursor()

    print("Initializing ImprvProfile")
    profile = ImprvProfile(start_date, end_date, cur)
    #can add filter_indices=
    profile.IMPRV_UNC = 0.7
    profile.save_bc("data/bc_profile")
    profile.IMPRV_UNC = 0.4
    profile.save_oc("data/oc_profile")
    profile.save_indices("data/profile_indices")
    
    print("Disconnecting from database.")
    cur.close()
    db.commit()
    db.close()
