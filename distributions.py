#!/usr/bin/env python
'''
This script constricts exponentially binned histograms of the 
distribution of concentrations in filtered IMPROVE data.
'''

import sqlite3
import numpy as np

#Connect to DB
db = sqlite3.connect("./data/sites.sqlite")
cur = db.cursor()

def get_carbon_array(sitecode):
    vals = cur.execute('''SELECT bc_val, oc_val FROM valid_imprv WHERE (site_code=? AND
            (bc_val>0 AND oc_val>0));''' , (sitecode,))
    return np.array(map(lambda t: (float(t[0]), float(t[1])), vals))


bins = [0.01 * 1.4**i for i in range(21)]
def distribution(carbon_array):
    return np.histogram(carbon_array, bins, density=False)


sites = list(map(lambda x: x[0], cur.execute(
    "SELECT DISTINCT site_code FROM valid_imprv WHERE lon>-130 AND lon<-110;")))


with file("data/distributions.txt", 'w') as outfile:
    for site in sites:
        carbon_array = get_carbon_array(site)
        bc_dist, edges = distribution(carbon_array[:,0])
        oc_dist= distribution(carbon_array[:,1])[0]  #discard edges; they'll be the same
        outfile.write("\n\n")
        outfile.write("\n".join(
            map(lambda x,y,z: "{} {} {}".format(x,y,z),
                edges[:-1], bc_dist, oc_dist )))


#Close DB connection
cur.close()
db.close()
