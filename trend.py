#!/usr/bin/python
'''
This script calculates and plots trends in IMPROVE data for just
the data I use in assimilations.
'''
import os
import numpy as np
from scipy.stats import linregress
from helpers import values_by_site, site_codes, lonlats, parse_date,\
    plt, m
import helpers as h

#Metadata
plt_dir = "./plt/trends/"

#Calculation
def relative_trend(vals):
    time_series = sorted(vals, key=lambda a: a[0]) #sort values by date
    start_date = time_series[0][0]
    start_value = time_series[0][1]
    dates = [ (data[0] - start_date).days for data in time_series]
    vals = [data[1] for data in time_series]
    slope, intercept, rval, pval, stderr = linregress(dates, vals)
    #because dates are dealt with in days, multiply trend by days/year
    #to get trend/year
    return slope*365.25/start_value*100 , stderr * 365.25 /start_value * 100


#Plotting
_markersize = lambda x: 0.1/x

def plot_trend_map(vals, title, cblabel, filename="tmp.png", **kwargs):
    '''Takes dates and values (e.g. bc or oc values) in a dict, performs
    trend analysis on each unique k/v pair (matching it with a lon/lat
    from the database) and plots the results on a pretty little
    map.'''

    data = {k: relative_trend(v) for k,v in vals.iteritems()}
    data = {k: (val, _markersize(err)) for k,(val, err) in data.iteritems()}
    h.imprv_plot(data, title, cblabel, filename, **kwargs)
    


if __name__ == "__main__":
    plot_trend_map(values_by_site(["date", "bc_val"]), 
                   "IMPROVE BC Trend", "%-change/yr", 
                   os.path.join(plt_dir, "imprv_bc_trend.png"),
                   vmin=-10, vmax=10,
                   cmap="RdBu")
    plot_trend_map(values_by_site(["date", "oc_val"]), 
                   "IMPROVE OC Trend", "%-change/yr", 
                   os.path.join(plt_dir, "imprv_oc_trend.png"),
                   vmin=-10, vmax=10,
                   cmap="RdBu")
