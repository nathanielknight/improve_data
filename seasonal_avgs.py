#!/usr/bin/python
'''
This script produces plots of seasonally-averaged IMPROVE data for the sites
that to into the BC and OC profiles.
'''

import itertools as it
import numpy as np
import helpers as h


def values_mean(kv):
    return {k:np.array([b for (a,b) in v]).mean() for k,v in kv.iteritems() if v}


_geo_mean = lambda x: np.sqrt(np.sum(x*x))/len(x)
def values_geometric_mean(kv):
    return {k:_geo_mean(np.array([b for (a,b) in v])) for k,v in kv.iteritems() if v}


def imprv_data(val, err, year):
    vals = values_mean(h.filtered_by_year(val, year))
    errs = values_geometric_mean(h.filtered_by_year(err, year))
    assert vals.keys() == errs.keys()

    data = {}
    for k in vals.keys():
        data[k] = (vals[k], errs[k]/vals[k] *0.65)
    return data


def plot(data, title, filename, **kwargs):
    h.imprv_plot(data, title, "ug/m^3",filename=filename,
                 vmin=0, **kwargs)

#data for export
profiles = {}

for spcs, year in it.product(["bc", "oc"], [1990, 1995, 2000, 2005]):
    val = spcs+"_val"
    err = spcs+"_unc"
    data = imprv_data(val, err, year)
    profiles[spcs+str(year)] = data
    vmax = {'oc':3.5, 'bc':0.6}[spcs]
    filename = "imprv_{}_{}.png".format(spcs, year)
    title = "IMPROVE {}, {}".format(spcs.upper(), year)

    plot(data, title, filename, vmax=vmax)
