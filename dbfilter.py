#!/usr/bin/python
'''
This script has two main sections.

The first creates the table `valid_imprv` which replaces bc and oc values below mdl with mdl/2.


The second filters out exceptionally high improve measurements (probably due to forest fires or other
localized emissions events).

If the improve data is lognormally distributed, it's logarithm is normally distributed. 
We take median (instead of mean) as mu, calculate, stdev exclude anything above mu+3sigma, 
and save the result.
'''

import sqlite3, sys
import numpy as np
import scipy.stats as stats
import datetime
import helpers as h

# Connect to database & grab a list of site-codes
db = sqlite3.connect("./data/sites.sqlite")
cur = db.cursor()
site_codes = map(lambda x: str(x[0]), cur.execute("SELECT DISTINCT site_code FROM raw_imprv;"))
urban_sites = ["ATLA1", "BALT1", "CHIC1", "DETR1", "FRES1", "HOUS1", "NEYO1",
               "OLTO1", "OMAH1", "PHOE1", "PITT1", "PUSO1", "RUBI1", "SAGU1",
               "WASH1"]


# Helpers
def try_float(a):
    try:
        return float(a)
    except ValueError:
        return a



# Filter below-mdl bc and oc ---------------------------------------------------
sys.stderr.write("Creating valid_imprv\n")
if not cur.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='valid_imprv'").fetchone():
    cur.execute('''CREATE TABLE valid_imprv AS
    SELECT * FROM raw_imprv WHERE
    bc_val>0 AND bc_unc>0 AND oc_val>0 AND oc_unc>0;''')
    cur.execute('''UPDATE valid_imprv SET bc_val=(bc_mdl/2) WHERE bc_val < bc_mdl''')
    cur.execute('''UPDATE valid_imprv SET oc_val=(oc_mdl/2) WHERE oc_val < oc_mdl''')


# Delete Urban Sites-----------------------------------------------------------
for s in urban_sites:
    cur.execute("""DELETE FROM valid_imprv WHERE site_code = ?""", [s]).fetchall()


# Delete sites that aren't in every year---------------------------------------
sys.stderr.write("Deleting intermittent sites\n")
valid_bc_sites = [s for s,v in  h.values_by_site(["date", "bc_val"]).iteritems()
                  if all(map(lambda y: h.check_for_year(v, y), [1990, 1995, 2000, 2005]))]

valid_oc_sites = [s for s,v in  h.values_by_site(["date", "oc_val"]).iteritems()
                  if all(map(lambda y: h.check_for_year(v,y), [1990, 1995, 2000, 2005]))]

valid_sites = set(valid_bc_sites).union(set(valid_oc_sites))
invalid_sites = set(site_codes) - valid_sites

for site_code in invalid_sites:
    sys.stderr.write("  {} has not enough data\n".format(site_code))
    cur.execute("DELETE FROM valid_imprv WHERE site_code = ?", [site_code]).fetchall()



# Filter extreme-value bc and oc------------------------------------------------
def logfilter(vals):
    x = np.log(vals)
    mu = np.median(x)
    sigma = np.std(x)
    cond = x < (mu + 3 * sigma)
# np.logical_and(
#         x > mu - 3 * sigma,
#        x < mu + 3 * sigma)
    newx = x[cond]
    return np.exp(newx)


def bc_values(sitecode):
    return np.array(map(lambda x: float(x[0]),
        cur.execute("SELECT bc_val FROM valid_imprv WHERE site_code=?", [sitecode]).fetchall()))

    
def oc_values(sitecode):
    return np.array(map(lambda x: float(x[0]),
        cur.execute("SELECT oc_val FROM valid_imprv WHERE site_code=?", [sitecode]).fetchall()))


def exceedences():
    for site_code in site_codes:
        bc_val = bc_values(site_code)
        oc_val = oc_values(site_code)
        yield site_code, len(bc_val) - len(logfilter(bc_val)), len(oc_val) - len(logfilter(oc_val))


total_points = sum([len(x) for x in map(bc_values, site_codes)])
exceeding_values = sum([len(x) - len(logfilter(x)) for x in map(bc_values, site_codes)])
print "Total points: {}".format(total_points)
print "Exceeding values: {}".format(exceeding_values)
print "percentage exceeding: {}".format(1.0 * exceeding_values / total_points * 100.0)


cur.close()
db.commit()
db.close()
