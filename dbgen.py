#!/usr/bin/env python
'''
This script extracts black carbon data from the IMPROVE network's CSV file and
puts it into a sqlite database for use by profile-generating scripts.  

It has 3 parts:
 * Location: creates a dict of (lat,lon) for the IMPROVE sites (from the .txt
   table in the /doc directory).

 * Database Creation: Creates the sites.sqlite database from the improve csv
   file in the /raw directory.

 * Selective data: Creates some persistent views which are more
   selective in terms of validation and replace values below MDL with MDL/2.
'''

from math import sqrt
import csv, sqlite3, os, sys


input_files = ["./raw/imprv_1990/data", "./raw/imprv_1995/data", 
               "./raw/imprv_2000/data", "./raw/imprv_2005/data"]



#Metadata  And Helpers-------------------------------------------------------------------
location_file = file("./doc/IMPROVELocTable.txt", 'r')
locations = {}
for line in location_file:
    sitecode, lat, lon = map(line.split().__getitem__, [0, 3, 4])
    lat, lon = float(lat), float(lon)
    locations[sitecode] = (lat, lon)

def make_index(header_fields):
    '''Index that each field will have in a string.split()'''
    return dict(zip(header_fields, range(len(header_fields))))

def try_float(x):
    try:
        return float(x)
    except ValueError:
        return x


#CSV Parser---------------------------------------------------------------------

"""
We're interested in black carbon and organic carbon, which IMPROVE records as EC
and OC. They're recorded in several separate fields (based on different stages
of the measurement process) which boil down to 

    ECf_val : fine elemental carbon
    OCf_val : fine organic carbon

The uncertainties  and MDL's aren't calculated in the CSV, so we need to do them
manually. The calculations are
    
    ECf_val = EC1+EC2+EC3-OP
    OCf_val = OC1+OC2+OC3+OC4+OP

The desired uncertainty is the geometric sum of the uncertainty on each
component. The desired MDL is the max (?) of the MDL on the components.

From examining the IMPROVE CSV file header, we can see what the relevant indices are.
"""

def csv_to_entry(csv_line, index):
    '''Extract the values desired by the adjoint from a line in the
    IMPROVE data csv file, prepared for insertion into the database.
    Takes an split list of qindex created with the make_index
    function.'''
    
    extract = lambda field : try_float(csv_line[index[field]]) 
    site_code = extract("SiteCode")
    date = extract("Date")
    
    try:
        lat, lon = locations[site_code]  #not stored in csv
    except KeyError:
        lat, lon = None, None


    bc_val = extract("ECf:Value")
    oc_val = extract("OCf:Value")

    bc_uncs = map(extract, ["EC1f:Unc", "EC2f:Unc", "EC3f:Unc", "OPf:Unc"])
    bc_unc = sqrt(sum(map(lambda x: x*x, bc_uncs)))

    oc_uncs = map(extract, ["OC1f:Unc", "OC2f:Unc", "OC3f:Unc", "OPf:Unc"])
    oc_unc = sqrt(sum(map(lambda x: x*x, oc_uncs)))

    bc_mdl = max(map(extract, ["EC1f:MDL", "EC2f:MDL", "EC3f:MDL", "OPf:Unc"]))
    oc_mdl = max(map(extract, ["OC1f:MDL", "OC2f:MDL", "OC3f:MDL", "OPf:Unc"]))

    return (date, site_code, lat, lon, bc_val, bc_unc, bc_mdl, oc_val, oc_unc, oc_mdl)


#Database Initialization--------------------------------------------------------------
#-- check for existing database

_msg = "'{}' exists; overwrite? (y/n)"
try:  #prompt user if database exists
    with open("data/sites.sqlite") as dbfile:
        while True:
            response = raw_input(_msg.format(dbfile.name))
            if response == 'y':
                sys.stderr.write(("Removing '{}'\n".format(dbfile.name)))
                os.remove(dbfile.name)
                break
            elif response == 'n':
                sys.stderr.write('Prudently refusing to continue in the face of existing database.\n')
                dbfile.close()
                exit()
except IOError:
    pass #file doesn't exist; carry on



#--connect to database
sys.stderr.write("Connecting to database\n")
db = sqlite3.connect("data/sites.sqlite")
cur = db.cursor()
#--create table with appropraite schema
sys.stderr.write("Creating table\n")
cur.execute('''CREATE TABLE raw_imprv 
(date text, site_code text, lat real, lon real, bc_val real, bc_unc real,
    bc_mdl real, oc_val real, oc_unc real, oc_mdl real, unique(date, site_code));
    ''')
cur.execute('''CREATE TABLE all_imprv 
(date text, site_code text, lat real, lon real, bc_val real, bc_unc real,
    bc_mdl real, oc_val real, oc_unc real, oc_mdl real);
    ''')


#Database Population from CSV----------------------------------------------------
#--open csv file with reader
for csv_file in input_files:
    sys.stderr.write("Reading IMPROVE data from {0}\n".format(csv_file))
    
    csvreader = csv.reader(file(csv_file, 'r'))
    ind = make_index(csvreader.next())
    for line in csvreader:
        entry = csv_to_entry(line, ind)
        if all(map(lambda x: x>=0, map(lambda x: entry[x], [4,5,6,7,8,9]))):
            try:
                cur.execute('INSERT INTO all_imprv VALUES (?,?,?,?, ?,?,?, ?,?,?);', entry)
                cur.execute('INSERT INTO raw_imprv VALUES (?,?,?,?, ?,?,?, ?,?,?);', entry)
            except sqlite3.IntegrityError as e: #tried to re-enter at an existing date-lat-lon
                sys.stderr.write("Duplicate location @: {}".format(entry))
        else:
            print("Denying invalid data: {}".format(entry))



#--save to database
sys.stderr.write("Saving to database and closing connection\n")
cur.close()
db.commit()
db.close()
