#!/usr/bin/python

import sqlite3

#Connect to database
db = sqlite3.connect("data/sites.sqlite")
cur = db.cursor()

def info(description, query):
    result = cur.execute(query).fetchone()
    description = description.rstrip() + '  '
    return "{0:-<40} : {1: >10}".format(description, result)


#Things of note
datums = [
        ("Start date", "SELECT MIN(date) FROM valid_imprv;"),
        ("End date", "SELECT MAX(date) FROM valid_imprv;"),
        ("Total entries", "SELECT COUNT(*) FROM valid_imprv;"),
        ("Min bc", "SELECT MIN(bc_val) FROM valid_imprv WHERE bc_val>0;"),
        ("Max bc", "SELECT MAX(bc_val) FROM valid_imprv WHERE bc_val>0;"),
        ("Avg bc", "SELECT AVG(bc_val) FROM valid_imprv WHERE bc_val>0;"),
        ("Non-Raw entries", '''SELECT COUNT(*) FROM valid_imprv WHERE
            bc_val>0 and bc_unc>0;'''),
        ("bc_val > bc_mdl", "SELECT COUNT(*) FROM valid_imprv WHERE bc_val > bc_mdl"),
        ("oc_val > oc_mdl", "SELECT COUNT(*) FROM valid_imprv WHERE oc_val > oc_mdl"),
        ("bc_unc>0", "SELECT COUNT(*) FROM valid_imprv WHERE bc_unc>0"),
        ("oc_unc>0", "SELECT COUNT(*) FROM valid_imprv WHERE oc_unc>0"),
        ("Site with the most data",
            '''SELECT site_code, n FROM (
                SELECT site_code, COUNT(*) AS n FROM valid_imprv GROUP BY
                site_code);'''),
        ]
for s in map(info, *zip(*datums)):
    print s

#Close and exit
cur.close()
db.close()
