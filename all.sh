#!/bin/bash

./profiles.py 19891201 19900228
./push_to_stetson.sh
ssh -p 23 natep@stetson.phys.dal.ca "cp profiles.zip /data2/natep/acshual_runs/gcadj_1990/runs/v8-02-01/geos4"

./profiles.py 19941201 19950228
./push_to_stetson.sh
ssh -p 23 natep@stetson.phys.dal.ca "cp profiles.zip /data2/natep/acshual_runs/gcadj_1995/runs/v8-02-01/geos4"

./profiles.py 19991201 20000228
./push_to_stetson.sh
ssh -p 23 natep@stetson.phys.dal.ca "cp profiles.zip /data2/natep/acshual_runs/gcadj_2000/runs/v8-02-01/geos4"

./profiles.py 20041201 20050228
./push_to_stetson.sh
ssh -p 23 natep@stetson.phys.dal.ca "cp profiles.zip /data2/natep/acshual_runs/gcadj_2005/runs/v8-02-01/geos4"
