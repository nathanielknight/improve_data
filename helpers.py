#!/usr/bin/python
import datetime, sqlite3
import numpy as np

#Plotting Concerns
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from mpl_toolkits.basemap import Basemap
from mpl_toolkits.axes_grid1 import make_axes_locatable
from gchem import grids as gc_grids










#Database Connection
db = sqlite3.connect("./data/sites.sqlite")
cur = db.cursor()


site_codes = map(lambda x: str(x[0]),
                 cur.execute("select distinct site_code from raw_imprv;"))



lonlats  = {k: cur.execute("select lon, lat from raw_imprv where site_code='{}' limit 1".format(k)).fetchone()
            for k in site_codes}

# Data parsing
def parse_date(a):
    if type(a) in [int, float]:
        y = int(a/1e4)
        m = int((a%1e4)/1e2)
        d = int(a%1e2)
        return datetime.date(y,m,d)
    elif type(a) == datetime.datetime:
        return a
    else:
        raise UserWarning, "parse_date doesn't know what to do with: {}".format(repr(a))

def longer_than(days, dmin):
    days = map(parse_date, days)
    return (max(days) - min(days)).days >= dmin

def try_float(a):
    try:
        return float(a)
    except ValueError:
        return a

def check_for_year(values, year):
    dates = [v[0] for v in values]
    return any(map(lambda x: (x.year == year) or (x.year == year -1), dates))

def print_data_existence(vals):
    print "site_code    1990  1995  2000  2005"
    tplt ="{: >5}        {}     {}     {}      {}"
    for site_code, values in values_by_site(vals).iteritems():
        presence = map(
            lambda y: '#' if check_for_year(values,y) else ' ',
            [1990, 1995, 2000, 2005])
        print tplt.format(site_code, *presence)

def filtered_by_year(val, year):
    return {k:filter(lambda (a,b): a.year==year-1 or a.year==year, v)
            for k,v in values_by_site(["date", val]).iteritems()}




#data access
def values_by_site(values):
    vals = {}
    for site_code in site_codes:
        query = "SELECT {} FROM VALID_IMPRV WHERE site_code = '{}';".format(
            ', '.join(values), site_code)
        result = cur.execute(query).fetchall()
        if result:
            vals[site_code] = [(parse_date(try_float(a)), try_float(b))
                               for (a,b) in result]
    return vals





#Plotting Concerns
m = Basemap(projection='eqdc', resolution='l',
            width=7000000, height=4500000,
            lat_1=45., lat_2=55., lat_0=40, lon_0=-100.)
_markersize=lambda a: [4.0 / x for x in a]
lon_edges, lat_edges = gc_grids.e_lon_4x5, gc_grids.e_lat_4x5

def imprv_plot(items, title, cblabel, filename="tmp.png", **kwargs):
    '''Plot a dict of {site_code: value, error} data just the way Randall
    likes it.'''
    data = {k:
            (lonlats[k][0], lonlats[k][1],
             items[k][0], items[k][1])
            for k in items.keys()}
    lons, lats, vals, errors = zip(*data.itervalues())

    plt.clf()
    ax = plt.subplot(1,1,1)
    m.drawcountries(color="gray")
    m.drawmeridians(lon_edges, color='g', labels=[False, False, False, True],
                     fontsize=8)
    m.drawparallels(lat_edges, color='g', labels=[True, False, False, False ],
                     fontsize=8)
    m.drawcoastlines(color="gray")
    m.drawstates(color="gray")
    plt.title(title)
    m.scatter(lons, lats, _markersize(errors), vals,
              marker='o',
              latlon=True,
              **kwargs)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cb=plt.colorbar(cax=cax)
    cb.set_label(cblabel)

    plt.savefig(filename, bbox_inches="tight")
