#!/usr/bin/env python
'''
This script draws a map of North America, overlays the GEOS-Chem grid,
and marks the location of each IMPROVE site.
'''

import sqlite3
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap

#Gather data
#--geos-chem grid
import gchem.grids as gcgrids
lon_edges, lat_edges = gcgrids.e_lon_4x5, gcgrids.e_lat_4x5

#--improve sites
db_file = "/home/neganp/work/improve_data/data/sites.sqlite"
db = sqlite3.connect(db_file)
curr = db.cursor()
sites = np.array(curr.execute("SELECT DISTINCT lon, lat FROM restricted_by_date;").fetchall())

#--world map



#Make a map, baby!
m = Basemap(projection='eqdc', resolution='l',
            width=7000000, height=4000000,
            lat_1=45., lat_2=55., lat_0=42., lon_0=-95.)
m.drawcoastlines()
m.drawcountries()
m.drawstates()
#--geos-chem grid
m.drawmeridians(lon_edges, color='g', labels=[False, False, False, True],
        fontsize=8)
m.drawparallels(lat_edges, color='g', labels=[True, False, False, False],
        fontsize=8)
#--improve sites
lons = filter(lambda x: bool(x), sites[:,0])
lats = filter(lambda y: bool(y), sites[:,1])
x,y = zip(*map(m, lons, lats))
m.plot(x, y, 'rx',markersize=2.0, label="IMPROVE sites")
#--labels and things
plt.legend()
plt.title("North American IMPROVE sites in the GEOS-Chem 4x5 Grid")

#Output
plt.savefig("plots/na_grid.png", dpi=180)
